<?php 

include "../processes/AddressDAO.php";

session_start();

if(isset($_SESSION["ip"])) {
    $ip = $_POST["ip"];
    $label = $_POST["label"];
    $process = new AddressDAO();
    if(empty($ip) || !filter_var($ip, FILTER_VALIDATE_IP)) {
        header("Location: ../list.php?err=1");
    } else {
        $process->addAddress($ip, $label);
        $process->auditTrail('POST', 'create', $ip, $_SESSION["ip"]);
        header("Location: ../list.php?ok=1");
    }
} else {
    session_destroy();
    header("Location: index.php?err=2");
}

