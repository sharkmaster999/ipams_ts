<?php 

include "../processes/AddressDAO.php";

session_start();

if(isset($_SESSION["ip"])) {
    $id = $_POST["id"];
    $ip = $_POST["ip"];
    $label = $_POST["label"];
    $process = new AddressDAO();
    $process->updateAddress($id, $label);
    header("Location: ../list.php?ok=2");
    $process->auditTrail('PUT', 'update', $ip, $_SESSION["ip"]);
} else {
    session_destroy();
    header("Location: index.php?err=2");
}