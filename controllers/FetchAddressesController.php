<?php 

include "processes/AddressDAO.php";

if(isset($_SESSION["ip"])) {
    $process = new AddressDAO();

    $data = $process->fetchAllAddresses();
    foreach($data as $row) {
        echo '<tr>';
        echo '<td>'. $row["ip"]. '</td>';
        echo '<td>'. $row["label"]. '</td>';
        echo '<td><button type="button" class="btn btn-info btn-xs upd-btn-trig" data-id="'. $row["id"]. '" data-ip="'. $row["ip"]. '" data-label="'. $row["label"]. '">Update</button></td>';
        echo '</tr>';
    }
} else {
    session_destroy();
    header("Location: index.php?err=2");
}

