<?php

include "../processes/AddressDAO.php";

session_start(); 

$process = new AddressDAO();
$process->auditTrail('GET', 'logout', $_SESSION["ip"], $_SESSION["ip"]);

session_destroy();

header("Location: ../index.php");