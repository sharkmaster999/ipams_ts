<?php 

include "../processes/AddressDAO.php";

session_start();

$ip = $_POST["ip"];

$process = new AddressDAO();

$data = $process->login($ip);

if($data != null) {
    $_SESSION["ip"] = $data["ip"];
    $process->auditTrail('GET', 'login', $data["ip"], $_SESSION["ip"]);
    header("Location: ../list.php");
} else {
    session_destroy();
    header("Location: ../?err=1");
}

