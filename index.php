<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="UTF-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <link rel="stylesheet" href="css/bootstrap.min.css">
        <title>IP Address Management Solutions</title>
    </head>
    <body>
        <div class="container">
            <br><br><br>
            <div class="row">
                <div class="col-md-4 col-md-offset-4">
                    <div class="well">
                        <h3>IP Address Management Solutions</h3>
                        <form action="controllers/LoginController.php" method="POST">
                            <div class="form-group">
                                <label for="#">IP Address:</label>
                                <input type="text" name="ip" id="ip" class="form-control" autocomplete="off">
                            </div>
                            <button type="submit" class="btn btn-primary btn-sm">Login</button>
                        </form>
                        <?php
                            if(isset($_GET["err"]) && $_GET["err"] != "") {
                                if($_GET["err"] == "1") {
                                    $err_msg = 'Entered IP Address invalid or empty';
                                } else if($_GET["err"] == "2") {
                                    $err_msg = 'Session expired. Please enter valid IP address again to begin.';
                                }
                                echo '<br><div class="alert alert-danger alert-sm"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>'. $err_msg. '</div>';
                            }
                        ?>
                    </div>
                </div>
            </div>
        </div>
        <script type="text/javascript" src="js/jquery-3.6.0.min.js"></script>
        <script type="text/javascript" src="js/bootstrap.min.js"></script>
    </body>
</html>