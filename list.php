<?php

session_start();

if(!isset($_SESSION["ip"])) {
    session_destroy();
    header("Location: index.php?err=2");
}
?>
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="UTF-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <link rel="stylesheet" href="css/bootstrap.min.css">
        <title>List | IP Address Management Solutions</title>
    </head>
    <body>
        <div class="container">
            <h3>IP Address Management Solutions</h3>
            <a class="btn btn-danger btn-sm" href="controllers/LogoutController.php">Log-out</a>
            <br><br>
            <div class="row">
                <div class="col-md-4">
                    <div class="well">
                        <h4><span class="headerLbl">Add New</span> Address</h4>
                        <form action="controllers/AddAddressController.php" method="POST" id="addr-frm">
                            <div class="form-group">
                                <label for="#ip_id">Address: </label>
                                <input type="hidden" name="id" id="ip_id" class="form-control">
                                <input type="text" name="ip" id="ip" class="form-control" autocomplete="off">
                            </div>
                            <div class="form-group">
                                <label for="#label">Label: </label>
                                <input type="text" name="label" id="label" class="form-control" autocomplete="off">
                            </div>
                            <button type="submit" class="btn btn-success btn-sm submit-btn">Add New</button> 
                            <button type="button" class="btn btn-danger btn-sm reset-btn" style="display: none;">Reset</button>
                        </form>
                        <?php
                            if(isset($_GET["ok"]) && $_GET["ok"] != "") {
                                if($_GET["ok"] == "1") {
                                    $ok_msg = 'IP address successfully added to the list.';
                                } else if($_GET["ok"] == "2") {
                                    $ok_msg = 'Selected IP address successfully updated.';
                                }
                                echo '<br><div class="alert alert-success alert-sm"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>'. $ok_msg. '</div>';
                            } else if(isset($_GET["err"]) && $_GET["err"] != "") {
                                if($_GET["err"] == "1") {
                                    $err_msg = 'IP address must be valid.';
                                }
                                echo '<br><div class="alert alert-danger alert-sm"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>'. $err_msg. '</div>';
                            }
                        ?>
                    </div>
                </div>
                <div class="col-md-8">
                    <table class="table table-bordered">
                        <thead>
                            <tr>
                                <th>Address</th>
                                <th>Label</th>
                                <th>Action</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php include "controllers/FetchAddressesController.php"?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
        <script type="text/javascript" src="js/jquery-3.6.0.min.js"></script>
        <script type="text/javascript" src="js/bootstrap.min.js"></script>
        <script type="text/javascript">
            $(function() {
                $(".upd-btn-trig").on("click", function() {
                    const id = $(this).data("id");
                    const ip = $(this).data("ip");
                    const label = $(this).data("label");
                    $("#ip_id").val(id);
                    $("#ip").val(ip);
                    $("#label").val(label);
                    $('#ip').attr('disabled', true);
                    $(".reset-btn").show();
                    $(".submit-btn").text("Update");
                    $(".headerLbl").text("Update");
                    $('#addr-frm').attr('action', 'controllers/UpdateAddressController.php');
                });

                $(".reset-btn").on("click", function() {
                    $("#ip_id").val("");
                    $("#ip").val("");
                    $("#label").val("");
                    $('#ip').attr('disabled', false);
                    $(this).hide();
                    $(".submit-btn").text("Add New");
                    $(".headerLbl").text("Add New");
                    $('#addr-frm').attr('action', 'controllers/AddAddressController.php');
                });
            });
        </script>
    </body>
</html>