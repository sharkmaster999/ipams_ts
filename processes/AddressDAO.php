<?php

include "ConnectionDAO.php";

class AddressDAO extends ConnectionDAO {
    function login($ip) {
        try {
            $res = null;
            $this->openConnection();
            $sql = "SELECT * FROM addresses WHERE ip = ?";
            $stmt = $this->dbh->prepare($sql);
            $stmt->bindParam(1, $ip);
            $stmt->execute();
            $res = $stmt->fetch(PDO::FETCH_ASSOC);
            $this->closeConnection();
            return $res;
        } catch (Exception $e) {
            $e->getMessage();
        }
    }

    function fetchAllAddresses() {
        try {
            $arr = null;
            $this->openConnection();
            $sql = "SELECT * FROM addresses";
            $stmt = $this->dbh->prepare($sql);
            $stmt->execute();
            
            while($row = $stmt->fetch(PDO::FETCH_ASSOC)) {
                $tmp_data = null;
                $tmp_data["id"] = $row["id"];
                $tmp_data["ip"] = $row["ip"];
                $tmp_data["label"] = $row["label"];
                $arr[] = $tmp_data;
            }

            $this->closeConnection();
            return $arr;
        } catch (Exception $e) {
            $e->getMessage();
        }
    }

    function addAddress($ip, $label) {
        try {
            $this->openConnection();
            $sql = "INSERT INTO addresses VALUES(0, ?, ?, 'now()', 'now()')";
            $stmt = $this->dbh->prepare($sql);
            $stmt->bindParam(1, $ip);
            $stmt->bindParam(2, $label);
            $stmt->execute();
            $this->closeConnection();
        } catch (Exception $e) {
            $e->getMessage();
        }
    }

    function updateAddress($id, $label) {
        try {
            $this->openConnection();
            $sql = "UPDATE addresses SET label = ?, updated_at = 'now()' WHERE id = ?";
            $stmt = $this->dbh->prepare($sql);
            $stmt->bindParam(1, $label);
            $stmt->bindParam(2, $id);
            $stmt->execute();
            $this->closeConnection();
        } catch (Exception $e) {
            $e->getMessage();
        }
    }

    function auditTrail($action_type, $action, $affected_ip, $operator_ip) {
        $file = fopen("../audit_trail.log", "a") or die("Unable to open file!");
        $txt = "[". date("l jS \of F Y h:i:s A"). "] + ". $action_type. "/ ". $action. " - [". $affected_ip. "] ". $operator_ip. "\n";
        fwrite($file, $txt);
        fclose($file);
    }
}